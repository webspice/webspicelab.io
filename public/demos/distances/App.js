import webspice from "./node_modules/webspice/dist/index.js";
import Resource from "./node_modules/webspice/dist/include/web/Resource.js";

class App {
  static getISOTime(date = new Date()) {
    let isoString = date.toISOString()
    let result = isoString.slice(0, 23);
    return result;
  }

  constructor(window, document) {
    this.window = window;
    this.document = document;
    this.statistics = {};
    this.shell = this.initialize();
  }

  get context() {
    let result = this.shell.then(function (shell) {
      let result = shell[shell.constructor.MODULECONTEXT];
      return result;
    });
    return result;
  }

  get heap() {
    let result = this.context.then(function (context) {
      return context.heap;
    });
    return result;
  }

  get fileStore() {
    let result = this.context.then(function (context) {
      return context.fileStore;
    });
    return result;
  }

  async initialize(includes) {
    let shell = await webspice.instantiate(includes);
    return shell;
  }

  async loadFile(path, onProgress) {
    const fileStore = await this.fileStore;
    let resource = new Resource(path);
    let file = await fileStore.persist(resource, function(progress) {
      onProgress(resource, progress);
    });
    return file;
  }
}

export default App;
