import Demo from "./Demo.js";
import BodyPairStats from "./BodyPairStats.js";
import * as includes from "./includes.js";

const kernels = {
  "ephemeris": "node_modules/webspice-kernel-de432s/naif/generic_kernels/spk/planets/de432s.bsp",
  "leapseconds": "node_modules/webspice-kernel-leapseconds/naif/generic_kernels/lsk/latest_leapseconds.tls"
};

const PRECISION = 16;

const
  EARTH = 399,
  SUN = 10,
  MOON = 301,
  MERCURY = 199,
  VENUS = 299
  ;

class Distances extends Demo {
  async initialize() {
    let shell = await super.initialize(includes);
    return shell;
  }

  async furnish(key) {
    const shell = await this.shell;
    const kernelPath = kernels[key];
    let result = shell.furnsh(kernelPath, (progress) => {
      this.onFileLoadProgress(kernelPath, progress);
    });
    return result;
  }

  async getBodyName(id) {
    const shell = await this.shell;
    let result = shell.bodc2n(id, 33);
    return result;
  }

  async getET(date = new Date()) {
    const shell = await this.shell;
    let strTime = this.constructor.getISOTime(date);
    let result = shell.str2et(strTime);
    return result;
  }

  async getTargetState(target = 0, observer = 0, et = 0) {
    const shell = await this.shell;
    let result = shell.spkez(target, et, "J2000", "NONE", observer);
    return result;
  }

  async getVectorNorm(vector) {
    const shell = await this.shell;
    let result = shell.vnorm(vector);
    return result;
  }

  layout() {
    const ui = this.ui;
    super.layout();

    let time = this.document.createElement("div");
    time.style.display = "none";
    ui.time = this.document.body.appendChild(time);
    let timeLabel = this.document.createElement("label");
    ui.timeLabel = ui.time.appendChild(timeLabel);
    ui.timeLabel.innerText = "Time (J2000): ";
    let timeValue = this.document.createElement("span");
    ui.timeValue = ui.time.appendChild(timeValue);

    ui.bodyPairs = [];
    ui.bodyPairs.push(new BodyPairStats(this, EARTH, SUN));
    ui.bodyPairs.push(new BodyPairStats(this, EARTH, MOON));
    ui.bodyPairs.push(new BodyPairStats(this, EARTH, MERCURY));
    ui.bodyPairs.push(new BodyPairStats(this, EARTH, VENUS));
    ui.bodyPairs.push(new BodyPairStats(this, SUN, EARTH));
    ui.bodyPairs.push(new BodyPairStats(this, SUN, MOON));
    ui.bodyPairs.push(new BodyPairStats(this, SUN, MERCURY));
    ui.bodyPairs.push(new BodyPairStats(this, SUN, VENUS));
  }

  async draw() {
    const ui = this.ui;
    super.draw();

    let et = await this.getET();
    let etFixed = et.toPrecision(PRECISION);
    ui.timeValue.innerText = `${etFixed}s`;

    await ui.bodyPairs.map((body) => body.draw(et));
  }

  async run() {
    const ui = this.ui;
    ui.fileLoaderLabel.innerText = "Loading kernels...";
    await this.furnish("ephemeris");
    await this.furnish("leapseconds");
    ui.time.style.display = "block";
    this.loop();
  }
}

export default Distances;
