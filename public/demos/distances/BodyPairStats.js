const PRECISION = 16;
const EARTH = 399;

class BodyPairStats {
  constructor(main, observer, target) {
    this.main = main;
    this.observer = observer;
    this.target = target;
    this.ui = {};
    this.layout();
  }

  layout() {
    const main = this.main;
    const ui = this.ui;

    let container = main.document.createElement("div");
    ui.container = main.document.body.appendChild(container);

    let distance = main.document.createElement("div");
    ui.distance = ui.container.appendChild(distance);
    let distanceLabel = main.document.createElement("label");
    ui.distanceLabel = ui.distance.appendChild(distanceLabel);
    let distanceValue = main.document.createElement("span");
    ui.distanceValue = ui.distance.appendChild(distanceValue);

    let speed = main.document.createElement("div");
    ui.speed = ui.container.appendChild(speed);
    let speedLabel = main.document.createElement("label");
    ui.speedLabel = ui.speed.appendChild(speedLabel);
    let speedValue = main.document.createElement("span");
    ui.speedValue = ui.speed.appendChild(speedValue);
  }

  async draw(et) {
    const main = this.main;
    const ui = this.ui;
    const target = this.target;
    const observer = this.observer;

    let targetName = await main.getBodyName(target);
    targetName = targetName ? targetName : "UNKNOWN";
    let observerName = await main.getBodyName(observer);
    observerName = observerName ? observerName : "UNKNOWN";

    let [starg, lt] = await main.getTargetState(target, observer, et);

    let position = starg.slice(0,3);
    let lightTime = lt;
    let distance = await main.getVectorNorm(position);
    let distanceFixed = distance.toPrecision(PRECISION);
    let lightTimeFixed = lightTime.toPrecision(PRECISION);
    ui.distanceLabel.innerText = `${observerName}-${targetName}, distance: `;
    ui.distanceValue.innerText =
     `${distanceFixed}km (${lightTimeFixed}lightseconds)`;

    let velocity = starg.slice(3,6);
    let speed = await main.getVectorNorm(velocity);
    let speedFixed = speed.toPrecision(PRECISION);
    ui.speedLabel.innerText = `${observerName}-${targetName}, speed: `;
    ui.speedValue.innerText = `${speedFixed}km/s`;
  }
}
export default BodyPairStats;
