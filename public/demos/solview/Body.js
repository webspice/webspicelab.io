class Body {
  constructor(main, id, title = "") {
    this.main = main;
    this.id = id;
    this.title = title;
    this.ui = {};
    this.layout();
  }

  layout() {
    const main = this.main;
    const ui = this.ui;
    const title = this.title;

    this.position = {x: 0, y: 0};

    let group = main.document.createElementNS("http://www.w3.org/2000/svg", "g");
    group.style.display = "none";
    ui.group = main.ui.output.appendChild(group);

    let icon = main.document.createElementNS("http://www.w3.org/2000/svg", "circle");
    icon.setAttribute("r", ".5%");
    icon.setAttribute("fill", "lightgray");
    ui.icon = ui.group.appendChild(icon);

    let label = main.document.createElementNS("http://www.w3.org/2000/svg", "text");
    label.setAttribute("alignment-baseline", "hanging");
    label.style.fill = "skyblue";
    label.style.fontFamily = "'Roboto', sans-serif";
    label.style.fontSize = "smaller";
    label.textContent = title;
    ui.label = ui.group.appendChild(label);
  }

  update(shell, et) {
    const main = this.main;
    const id = this.id;

    let [starg, lt] = shell.spkez(id, et, "J2000", "NONE", 0);
    let position = starg.slice(0,3);

    this.position.x = position[0];
    this.position.y = position[1];
  }

  get anchorOffset() {
    const ui = this.ui;
    let bbox = ui.group.getBBox();
    let offsetX = ui.icon.getAttribute("cx") - bbox.x;
    let offsetY = ui.icon.getAttribute("cy") - bbox.y;
    let result = [offsetX, offsetY];
    return result;
  }

  predictBBox(anchorX, anchorY) {
    const ui = this.ui;
    let bbox = ui.group.getBBox();
    let anchorOffset = this.anchorOffset;

    let x = anchorX - anchorOffset[0];
    let y = anchorY - anchorOffset[1];
    let width = bbox.width;
    let height = bbox.height;

    let result = {x, y, width, height};
    return result;
  }

  predictMaxBoundingError(anchorX, anchorY) {
    const main = this.main;
    const viewport = main.viewport;

    let viewportWidth = viewport.viewBox[2];
    let viewportHeight = viewport.viewBox[3];

    let leftBound = viewport.viewBox[0];
    let topBound = viewport.viewBox[1];
    let rightBound = leftBound + viewportWidth;
    let bottomBound = topBound + viewportHeight;

    let bbox = this.predictBBox(anchorX, anchorY);
    let left = bbox.x;
    let top = bbox.y;
    let right = left + bbox.width;
    let bottom = top + bbox.height;

    let leftError = (leftBound - left) / Math.abs(leftBound);
    let topError = (topBound - top) / Math.abs(topBound);
    let rightError = (right - rightBound) / Math.abs(rightBound);
    let bottomError = (bottom - bottomBound) / Math.abs(bottomBound);

    let result = Math.max(leftError, topError, rightError, bottomError);
    return result;
  }

  predictCurrentMaxBoundingError(scale = 1) {
    const main = this.main;
    let projectedX = main.project(this.position.x, scale);
    let projectedY = main.project(this.position.y, scale);
    let result = this.predictMaxBoundingError(projectedX, projectedY);
    return result;
  }

  async draw(scale = 1) {
    const main = this.main;
    const ui = this.ui;

    let projectedX = main.project(this.position.x, scale);
    let projectedY = main.project(this.position.y, scale);

    ui.icon.setAttribute("cx", projectedX);
    ui.icon.setAttribute("cy", projectedY);

    let iconBBox = ui.icon.getBBox();
    ui.label.setAttribute("x", iconBBox.x + iconBBox.width);
    ui.label.setAttribute("y", iconBBox.y + iconBBox.height);

    ui.group.style.display = "";
  }
}

export default Body;
