import Demo from "./Demo.js";
import Body from "./Body.js";
import * as includes from "./includes.js";

const kernels = {
  "ephemeris": "node_modules/webspice-kernel-de432s/naif/generic_kernels/spk/planets/de432s.bsp",
  "leapseconds": "node_modules/webspice-kernel-leapseconds/naif/generic_kernels/lsk/latest_leapseconds.tls"
};

const PRECISION = 16;

class SolView extends Demo {
  async initialize() {
    let shell = await super.initialize(includes);
    return shell;
  }

  async furnish(key) {
    const shell = await this.shell;
    const kernelPath = kernels[key];
    let result = shell.furnsh(kernelPath, (progress) => {
      this.onFileLoadProgress(kernelPath, progress);
    });
    return result;
  }

  async getET(date = new Date()) {
    const shell = await this.shell;
    let strTime = this.constructor.getISOTime(date);
    let result = shell.str2et(strTime);
    return result;
  }

  calculateViewport() {
    const ui = this.ui;
    const output = ui.output;

    let viewBox = [];
    viewBox[0] = (output.clientWidth / 2) * -1;
    viewBox[1] = (output.clientHeight / 2) * -1;
    viewBox[2] = output.clientWidth;
    viewBox[3] = output.clientHeight;

    let unit = Math.max(Math.abs(viewBox[0]), Math.abs(viewBox[1]));

    let result = {viewBox, unit};
    return result;
  }

  layout() {
    const ui = this.ui;
    super.layout();
    ui.statistics.style.color = "white";

    let output = document.getElementById("output");
    ui.output = output;

    ui.bodies = [];
    ui.bodies.push(new Body(this, 0, "Sun"));
    ui.bodies.push(new Body(this, 1, "Mercury"));
    ui.bodies.push(new Body(this, 2, "Venus"));
    ui.bodies.push(new Body(this, 3, "Earth"));
    ui.bodies.push(new Body(this, 4, "Mars"));
  }

  update(shell, et) {
    const ui = this.ui;
    const output = ui.output;

    this.viewport = this.calculateViewport();
    let viewBoxSerialized = this.viewport.viewBox.join()
    output.setAttribute("viewBox", viewBoxSerialized);

    ui.bodies.map((body) => body.update(shell, et));

    this.space = {};
    this.space.rangeX = ui.bodies.reduce(
      (result, body) => Math.max(Math.abs(body.position.x), result),
      0
    );
    this.space.rangeY = ui.bodies.reduce(
      (result, body) => Math.max(Math.abs(body.position.y), result),
      0
    );
    this.space.range = Math.max(this.space.rangeX, this.space.rangeY);
  }

  project(value, scale = 1) {
    const range = this.space.range;
    const unit = this.viewport.unit;
    let fraction = value / range || 0;
    let scaled = fraction * scale;
    let result = scaled * unit;
    return result;
  }

  async draw() {
    const ui = this.ui;
    super.draw();

    let shell = await this.shell;
    let et = await this.getET();
    //let etFixed = et.toPrecision(PRECISION);
    //ui.timeValue.innerText = `${etFixed}s`;

    this.update(shell, et);
    let maxBoundingError = ui.bodies.reduce(
      (result, body) => Math.max(Math.abs(body.predictCurrentMaxBoundingError()), result),
      0
    );
    let scale = 1 / maxBoundingError;
    for (let body of ui.bodies) {
      await body.draw(scale);
    }
  }

  async run() {
    const ui = this.ui;
    ui.fileLoaderLabel.innerText = "Loading kernels...";
    await this.furnish("ephemeris");
    await this.furnish("leapseconds");
    this.loop();
  }
}

export default SolView;
