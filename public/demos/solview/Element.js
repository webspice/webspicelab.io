class Element {
  constructor(main, name) {
    this.main = main;
    this.node = main.document.createElementNS("http://www.w3.org/2000/svg", name);
  }
}
